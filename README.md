# راهنما تدوین پروژه آزمایشگاه مهندسی نرم‌افزار

این مستندات برای راهنمایی دانشجویان عزیز برای تدوین و راه اندازی رپازیتوری پروژه آزمایشگاه مهندسی نرم‌افزار با هدف آشنایی بیشتر با گیت و ابزارهای مختلف آن تهیه می‌شود.

دانشجویان می‌توانند از قالب تعریف شده برای دیگر پروژه های نرم‌افزاری استفاده کنند. 

## قرارداد ها:
+ مستندات پروژه می‌توانند به زبان فارسی یا انگلیسی باشند.
+ علامت :exclamation: مشخص کننده الزامی بودن یک مورد است و باید طبق فرمت تعریف شده انجام شوند.
+ علامت :rocket: مشخص کننده اختیاری بودن یک مورد است.

## ساختار پروژه

+  مستندات نوشتاری پروژه باید در فرمت `markdown`  و مستندات تصویری باید   با فرمت `jpeg` ،  `png`  یا `MarkdownGraph`  در پوشه `documentation`  که در ریشه پروژه قرار دارد منتشر شوند. :exclamation:
+  در پوشه اصلی و پوشه `documentation` باید فایلهای `README.MD`  ساخته شوند تا توضیحات و ارجاعات مربوطه در درون آنها قرار داده شود. :exclamation:
+ پوشه `test-case` می تواند برای تعریف تست های مختلف نرم افزار ساخته شود. :rocket:



[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggTFJcblxuICAgIEFbXCJcXFwicHJvamVjdC1yb290XVxuICAgICBBID09PiBCKFwiXFxcImRvY3VtZW50YXRpb24pXG4gICAgc3ViZ3JhcGggZG9jdW1lbnRhdGlvIGRpcmVjdG9yeVxuICAgXG4gICAgQiAtLT4gQyhcIlxcXCJpbWFnZXMpXG4gICAgQiAtLT4gfCBNYXJrZG93biBmaWxlcyAuLi4gfERbKi5tZF1cbiAgICBCIC0tPiBQKFJFQURNRS5NRClcbiAgICBlbmRcblxuICAgIEEgLS0-IEcoUkVBRE1FLk1EKVxuICAgIEEgLS0-IEVbcHJvamVjdCBzb3VyY2UgZmlsZXMgXVxuICAgIEEgPT0-IEYoXCJcXFwidGVzdC1jYXNlcyAqb3B0aW9uYWwqKVxuXG4gICAgXG4gICAgXG4gICAgc3R5bGUgQSBmaWxsOm9yYW5nZVxuICAgIHN0eWxlIEMgZmlsbDogIzNmNVxuICAgIHN0eWxlIEIgZmlsbDogIzNmNVxuICAgIHN0eWxlIEYgZmlsbDogIzNmNVxuICAgICIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggTFJcblxuICAgIEFbXCJcXFwicHJvamVjdC1yb290XVxuICAgICBBID09PiBCKFwiXFxcImRvY3VtZW50YXRpb24pXG4gICAgc3ViZ3JhcGggZG9jdW1lbnRhdGlvIGRpcmVjdG9yeVxuICAgXG4gICAgQiAtLT4gQyhcIlxcXCJpbWFnZXMpXG4gICAgQiAtLT4gfCBNYXJrZG93biBmaWxlcyAuLi4gfERbKi5tZF1cbiAgICBCIC0tPiBQKFJFQURNRS5NRClcbiAgICBlbmRcblxuICAgIEEgLS0-IEcoUkVBRE1FLk1EKVxuICAgIEEgLS0-IEVbcHJvamVjdCBzb3VyY2UgZmlsZXMgXVxuICAgIEEgPT0-IEYoXCJcXFwidGVzdC1jYXNlcyAqb3B0aW9uYWwqKVxuXG4gICAgXG4gICAgXG4gICAgc3R5bGUgQSBmaWxsOm9yYW5nZVxuICAgIHN0eWxlIEMgZmlsbDogIzNmNVxuICAgIHN0eWxlIEIgZmlsbDogIzNmNVxuICAgIHN0eWxlIEYgZmlsbDogIzNmNVxuICAgICIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2V9)

## فرمت فایلها:

+ فرمت فایل `README.md` در پوشه اصلی پروژه [لینک](/HOME-README.md) :exclamation:


## دیگر لینک ها

+ آموزش MARKDOWN فارسی: [لینک](https://virgool.io/@aliiranmanesh/httpsvirgoolioaliiranmaneshtutorial-markdown-sro3zufagdxk)
+ آموزش MARKDOWN انگلیسی: [لینک](https://www.markdowntutorial.com/)
+  ادیتور فارسی *مرتب* [لینک](https://www.sobhe.ir/moratab/)
+  ادیتور MarkdwonGraph [پیوند](https://mermaid-js.github.io/)
+  لیستی از شکلک ها(emoji) برای MARKDOWN  [پیوند](https://github.com/ikatyang/emoji-cheat-sheet)
